﻿using LiteCommerce.DataLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.BusinessLayers
{
    
    public static class CatelogBLL
    {
        public static void Initalize(string connectionString)
        {
            SupplierDB = new DataLayers.SQLServer.SupplierDAL(connectionString);
            CustomerDB = new DataLayers.SQLServer.CustomerDAL(connectionString);
            CategoryDB = new DataLayers.SQLServer.CategoryDAL(connectionString);
            ShipperDB = new DataLayers.SQLServer.ShipperDAL(connectionString);
            EmployeeDB = new DataLayers.SQLServer.EmloyeeDAL(connectionString);
            ProductDB = new DataLayers.SQLServer.ProductDAL(connectionString);
            AttributeDB = new DataLayers.SQLServer.AttributeDAL(connectionString);
            CountryDB = new DataLayers.SQLServer.CountryDAL(connectionString);
            OrderDB = new DataLayers.SQLServer.OrderDAL(connectionString);

        }
        private static ISupplierDAL SupplierDB { get; set; }
        private static ICustomerDAL CustomerDB { get; set; }
        private static ICategoryDAL CategoryDB { get; set; }
        private static IShipperDAL ShipperDB { get; set; }
        private static IEmployeeDAL EmployeeDB{ get; set; }
        private static IProductDAL ProductDB { get; set; }
        private static IAttributeDAL AttributeDB { get; set; }
        public static ICountryDAL CountryDB { get; set; }
        public static IOrderDAL OrderDB { get; set; }
        
        public static List<Supplier> ListOfSuppliers(int page, int pageSize, string searchValue, out int rowCount) {
            if (page < 1)
                page = 1;
            if (pageSize < 0)
                pageSize = 20;
            
            rowCount = SupplierDB.Count(searchValue);
            return SupplierDB.List(page, pageSize, searchValue);
        }
        public static List<Customer> ListOfCustomers(int page, int pageSize, string searchValue, out int rowCount,string nameCountry)
        {
            if (page < 1)
                page = 1;
            if (pageSize < 0)
                pageSize = 20;

            rowCount = CustomerDB.Count(searchValue, nameCountry);
            return CustomerDB.List(page, pageSize, searchValue, nameCountry);

        }
        public static List<Category> ListOfCategories(int page, int pageSize, string searchValue, out int rowCount)
        {
            if (page < 1)
                page = 1;
            if (pageSize < 0)
                pageSize = 20;

            rowCount = CategoryDB.Count(searchValue);
            return CategoryDB.List(page, pageSize, searchValue);
        }
        public static List<Shipper> ListOfShippers(int page, int pageSize, string searchValue, out int rowCount)
        {
            if (page < 1)
                page = 1;
            if (pageSize < 0)
                pageSize = 20;

            rowCount = ShipperDB.Count(searchValue);
            return ShipperDB.List(page, pageSize, searchValue);

        }
        public static List<Employee> ListOfEmployees(int page, int pageSize, string searchValue, out int rowCount)
        {
            if (page < 1)
                page = 1;
            if (pageSize < 0)
                pageSize = 20;

            rowCount = EmployeeDB.Count(searchValue);
            return EmployeeDB.List(page, pageSize, searchValue);

        }
        public static List<Product> ListOfProduct(int page, int pageSize, string searchValue, out int rowCount,string categoryId,string supplierId)
        {
            if (page < 1)
                page = 1;
            if (pageSize < 0)
                pageSize = 20;
            if (categoryId.Equals("0"))
                categoryId = "";
            if (supplierId.Equals("0"))
                supplierId = "";
            rowCount = ProductDB.Count(searchValue,categoryId,supplierId);
            return ProductDB.List(page, pageSize, searchValue,categoryId,supplierId);

        }
       
        public static Supplier GetSupplier(int supplierID)
        {
            return SupplierDB.Get(supplierID);
        }
        /// <summary>
        /// Thêm 1 supplier
        /// </summary>
        /// <param name="data">1 supplier</param>
        /// <returns></returns>
        public static int AddSupplier(Supplier data)
        {
            return SupplierDB.Add(data);
        }
        /// <summary>
        /// Chỉnh sủa 1 supplier
        /// </summary>
        /// <param name="data"> 1 supplier</param>
        /// <returns></returns>
        public static bool UpdateSupplier(Supplier data)
        {
            return SupplierDB.Update(data);
        }
        public static int DeleteSuppliers(int[] supplierIDs)
        {
            return SupplierDB.Delete(supplierIDs);
        }
        public static Category GetCategory(int categoryID)
        {
            return CategoryDB.Get(categoryID);
        }
        /// <summary>
        /// Thêm 1 supplier
        /// </summary>
        /// <param name="data">1 supplier</param>
        /// <returns></returns>
        public static int AddCategory(Category data)
        {
            return CategoryDB.Add(data);
        }
        /// <summary>
        /// Chỉnh sủa 1 supplier
        /// </summary>
        /// <param name="data"> 1 supplier</param>
        /// <returns></returns>
        public static bool UpdateCategory(Category data)
        {
            return CategoryDB.Update(data);
        }
        public static int DeleteCategories(int[] categoryIDs)
        {
            return CategoryDB.Delete(categoryIDs);
        }
        public static int CountCategory()
        {
            return CategoryDB.Count("");
        }

        /// <summary>
        /// GET ONE CUSTOMER
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public static Customer GetCustomer(string customerID)
        {
            return CustomerDB.Get(customerID);
        }
        /// <summary>
        /// Thêm 1 Customer
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static int AddCusstomer(Customer data)
        {
            return CustomerDB.Add(data);
        }
        public static bool UpdateCustomer(Customer data)
        {
            return CustomerDB.Update(data);
            
        }
        public static int DeleteCustomer(string[] customerIDs)
        {
            return CustomerDB.Delete(customerIDs);
        }
        public static int CountCustomer()
        {
            return CustomerDB.Count("","");
        }



        /* SHIPPER */
        /// <summary>
        /// Lấy 1 Shipper 
        /// </summary>
        /// <param name="shipperID"></param>
        /// <returns></returns>
        public static Shipper GetShipper(int shipperID)
        {
            return ShipperDB.Get(shipperID);
        }
        /// <summary>
        /// thêm 1 Shipper
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static int AddShipper(Shipper data)
        {
            return ShipperDB.Add(data);
        }
        /// <summary>
        /// xóa 1 danh sách Shipper
        /// </summary>
        /// <param name="ShipperIDs"></param>
        /// <returns></returns>
        public static int DeleteShippers(int[] ShipperIDs)
        {
            return ShipperDB.Delete(ShipperIDs);
        }
        /// <summary>
        /// update 1 Shipper
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool UpdateShipper(Shipper data)
        {
            return ShipperDB.Update(data);
        }
        public static int CountShipper()
        {
            return ShipperDB.Count("");
        }


        /* EMPLOYEE*/
        /// <summary>
        /// Lấy 1 Employee 
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <returns></returns>
        public static Employee GetEmployee(int EmployeeID)
        {
            return EmployeeDB.Get(EmployeeID);
        }
        /// <summary>
        /// thêm 1 Employee
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static int AddEmployee(Employee data)
        {
            return EmployeeDB.Add(data);
        }
        /// <summary>
        /// xóa 1 danh sách Employee
        /// </summary>
        /// <param name="EmployeeIDs"></param>
        /// <returns></returns>
        public static int DeleteEmployees(int[] EmployeeIDs)
        {
            return EmployeeDB.Delete(EmployeeIDs);
        }
        /// <summary>
        /// update 1 Employee
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool UpdateEmployee(Employee data)
        {
            return EmployeeDB.Update(data);
        }

        public static bool ChangePassword(string email, string oldPass, string newPass)
        {
            return EmployeeDB.ChangePassword(email,oldPass, newPass);
        }

        public static Employee GetInfo(string email,string password)
        {
            return EmployeeDB.GetInfoUserByEmailAndPass(password, email);
        }
        public static int CountEmployee()
        {
            return EmployeeDB.Count("");
        }

        /* Categories */
        public static List<Category> getAllCategories()
        {
            return CategoryDB.getList();
        }
        public static int CountCategories()
        {
            return CategoryDB.Count("");
        }

        /* Supplier */
        public static List<Supplier> getAllSuppliers()
        {
            return SupplierDB.getList();
        }
        public static int CountSupplier()
        {
            return SupplierDB.Count("");
        }


        /* Attribute */
        public static List<DomainModels.Attribute> GetAttributeByCategoryId(int categoryID)
        {
            return AttributeDB.GetAttributesByIdCategory(categoryID);
        }
        public static List<DomainModels.ProductAttribute> GetAttrByCategoryIdAndProductId(int categoryID, int productID)
        {
            return AttributeDB.GetAttributesByIdCategoryAndIdProduct(categoryID,productID);
        }
        public static int deleteProductAttribute(int productID)
        {
            return AttributeDB.DeleteAttributeProduct(productID);
        }

        public static int DeleteAttributeByCategoryId(int categoryId)
        {
            return AttributeDB.DeleteAttributeByCategory(categoryId);
        }
        public static int AddAttributeCategory(int categoryId,string[] attrName)
        {
            if(attrName.Length > 0)
            {
                foreach(string name in attrName)
                {
                    AttributeDB.AddAttribute(categoryId, name);
                }
                return 0;
            }
            return 1;

        }

        /* Product */
        public static Product GetProduct(int ProductID)
        {
            return ProductDB.Get(ProductID);
        }
        public static int AddProduct(Product product)
        {
            return ProductDB.Add(product);
        }
        public static int AddProductAttribute(string[] idAttributes, string[] attrValues,int ProductID)
        {
            if(idAttributes.Length == attrValues.Length)
            {
                for(int i = 0; i < idAttributes.Length; i++)
                {
                   AttributeDB.AddAttributeProduct(Convert.ToInt32(idAttributes[i]), attrValues[i], ProductID);
                }
                return 0;
            }else
            {
                return 1;
            }
            
        }
        public static void UpdateProduct(Product product, string[] idAttributes, string[] attrValues)
        {
            ProductDB.Update(product);
            AttributeDB.DeleteAttributeProduct(product.ProductID);
            AddProductAttribute(idAttributes, attrValues, product.ProductID);
        }
        public static void DeleteProduct(int[] productIds)
        {
            
            foreach(int idProduct in productIds)
            {
                AttributeDB.DeleteAttributeProduct(idProduct);
            }
            ProductDB.Delete(productIds);
        }
        public static int CountProduct()
        {
            return ProductDB.Count("", "", "");
        }

        public static List<Product> Top5ProductSelling()
        {
            return ProductDB.GetTop5SellingProducts();
        }

        /* Country */
        public static List<Country> GetList()
        {
            return CountryDB.getAll();
        }
        public static List<Country> GetListCountries(int page, int pageSize, string searchValue, out int rowCount)
        {
            if (page < 1)
                page = 1;
            if (pageSize < 0)
                pageSize = 20;
            rowCount = CountryDB.Count(searchValue);
            return CountryDB.List(page, pageSize, searchValue);
        }

        public static int DeleteCountry(int[] countries)
        {
            return CountryDB.Delete(countries);
        }
        public static int AddCountry(Country data)
        {
            return CountryDB.Add(data);
        }
        public static bool UpdateCountry(Country data)
        {
            return CountryDB.Update(data);
        }
        public static List<Country> ListOfCountry(int page, int pageSize, string searchValue, out int rowCount)
        {
            if (page < 1)
                page = 1;
            if (pageSize < 0)
                pageSize = 20;
            rowCount = CountryDB.Count(searchValue);
            return CountryDB.List(page, pageSize, searchValue);
        }
        public static Country GetCountry(int CountryID)
        {
            return CountryDB.Get(CountryID);
        }

        /* ORDER */

        // GET ALL ORDER CAST ORDER DETAILS
        public static List<Order> ListOfOrder(int page, int pageSize, string searchValue, out int rowCount)
        {
            if (page < 1)
                page = 1;
            if (pageSize < 0)
                pageSize = 20;
            rowCount = OrderDB.Count(searchValue);
            List<Order> orders = new List<Order>();
            OrderDB.List(page, pageSize, searchValue).ForEach(rs =>
            {
                List<OrderDetails> orderDetailsList = new List<OrderDetails>();
                rs.Customer = CustomerDB.Get(rs.CustomerID);
                rs.Employee = EmployeeDB.Get(rs.EmployeeID);
                rs.Shipper = ShipperDB.Get(rs.ShipperID);
                List<OrderDetails> ord = OrderDB.GetDetailsByOrderId(rs.OrderID);
                ord.ForEach(x =>
                {
                    x.Product = ProductDB.Get(x.ProductID);
                    orderDetailsList.Add(x);
                });
                rs.OrderDetails = ord;
                orders.Add(rs);
            });
            return orders;

        }
        public static Order GetDetailsOrder(int orderId)
        {
            Order order = OrderDB.Get(orderId);
            List<OrderDetails> orderDetailsList = new List<OrderDetails>();
            order.Customer = CustomerDB.Get(order.CustomerID);
            order.Employee = EmployeeDB.Get(order.EmployeeID);
            order.Shipper = ShipperDB.Get(order.ShipperID);
            List<OrderDetails> ord = OrderDB.GetDetailsByOrderId(order.OrderID);
            ord.ForEach(x =>
            {
                x.Product = ProductDB.Get(x.ProductID);
                orderDetailsList.Add(x);
            });
            order.OrderDetails = ord;
            return order;

        }
        public static int CountOrder()
        {
            return OrderDB.Count("");
        }
        public static List<int> GetYearOrder()
        {
            return OrderDB.GetYearOrder();
        }
        public static List<OrderYearly> GetOrderYearly(int year)
        {
            return OrderDB.GetOrderYearly(year);
        }
    }
}
