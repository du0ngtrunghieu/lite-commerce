﻿using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LiteCommerceAdmin.Models
{
    public class DashBoardModel
    {
        public int countCustomers { get; set; }
        public int countProducts { get; set; }
        public int countOrders { get; set; }
        public int countEmployees { get; set; }
        public int countSuppliers { get; set; }
        public int countShippers { get; set; }
        public List<Product> Top5Selling { get; set; }
    }
}