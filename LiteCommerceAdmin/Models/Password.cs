﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LiteCommerceAdmin.Models
{
    public class Password<T>
    {
        public int Id { get; set; }
        public string  Email { get; set; }
        public string NewPass { get; set; }
        public string OldPass { get; set; }
        public string ConfirmPass { get; set; }
    }
}