﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LiteCommerceAdmin
{
    public class WebUserData
    {
        /// <summary>
        /// ID/tên đăng nhập của tài khoản
        /// </summary>
        public string UserID { get; set; }
        /// <summary>
        /// Tên gọi/tên hiển thị của tài khoản
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// Tên nhóm
        /// </summary>
        public List<string> GroupName { get; set; }
        /// <summary>
        /// Thời điểm đăng nhập
        /// </summary>
        public DateTime LoginTime { get; set; }
        /// <summary>
        /// Session ID
        /// </summary>
        public string SessionID { get; set; }
        /// <summary>
        /// Địa chỉ IP của user khi đăng nhập
        /// </summary>
        public string ClientIP { get; set; }
        /// <summary>
        /// Ảnh
        /// </summary>
        public string Photo { get; set; }

        /// <summary>
        /// Chuyển thông tin tài khoản đăng nhập thành chuỗi để ghi Cookie
        /// </summary>
        /// <returns></returns>
        public string ToCookieString()
        {
            return string.Format($"{UserID}|{FullName}|{string.Join(",",GroupName)}|{LoginTime}|{SessionID}|{ClientIP}|{Photo}|{Title}|{Email}");
        }

        /// <summary>
        /// Lấy thông tin tài khoản đăng nhập từ Cookie
        /// </summary>
        /// <param name="cookie"></param>
        /// <returns></returns>
        public static WebUserData FromCookieString(string cookie)
        {
            try
            {
                string[] infos = cookie.Split('|');
                if (infos.Length == 9)
                {
                    var t = infos[2];
                    return new WebUserData()
                    {
                        UserID = infos[0],
                        FullName = infos[1],
                        GroupName = infos[2].Split(',').ToList(),
                        LoginTime = Convert.ToDateTime(infos[3]),
                        SessionID = infos[4],
                        ClientIP = infos[5],
                        Photo = infos[6],
                        Title = infos[7],
                        Email = infos[8],
                    };
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }
        public string Title { get; set; }
        public string Email { get; set; }
        
    }
}