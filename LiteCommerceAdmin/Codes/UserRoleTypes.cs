﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LiteCommerceAdmin
{
    public static class UserRoleTypes
    {
        public const string ROLE_SALE = "ROLE_SALE";
        public const string ROLE_CATALOG = "ROLE_CATALOG";
        public const string ROLE_MANAGER = "ROLE_MANAGER";
    }
}