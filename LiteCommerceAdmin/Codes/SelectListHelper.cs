﻿using LiteCommerce.BusinessLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerceAdmin
{
    public static class SelectListHelper
    {
        public static List<SelectListItem> Countries(bool allSelectAll = true)
        {
            List<Country> getList = CatelogBLL.GetList();
            List<SelectListItem> List = new List<SelectListItem>();
           
            foreach (var country in getList)
            {
                List.Add(new SelectListItem { Value = country.Abbreviation, Text = country.CountryName });
            }
            return List;
        }
        public static List<SelectListItem> Categories(bool allowSelectAll = true)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (allowSelectAll)
            {
                list.Add(new SelectListItem() { Value = "0", Text = "--- All Categories ---" });
                List<Category> listOfCategory = CatelogBLL.getAllCategories();
                foreach (Category cat in listOfCategory)
                {
                    list.Add(new SelectListItem() { Value = cat.CategoryID.ToString(), Text = cat.CategoryName });
                };
            }
            return list;
        }
        public static List<SelectListItem> Suppliers(bool allowSelectAll = true)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (allowSelectAll)
            {
                list.Add(new SelectListItem() { Value = "0", Text = "--- All Suppliers ---" });
                List<Supplier> listOfSupplier = CatelogBLL.getAllSuppliers();
                foreach (Supplier sup in listOfSupplier)
                {
                    list.Add(new SelectListItem() { Value = sup.SupplierID.ToString(), Text = sup.CompanyName });

                }
            }
            return list;
        }
        public static List<LiteCommerce.DomainModels.Attribute> Attributes(int CategoryID)
        {
            List<LiteCommerce.DomainModels.Attribute> listofAttribute = new List<LiteCommerce.DomainModels.Attribute>();
            if (CategoryID > 0)
            {
                listofAttribute = CatelogBLL.GetAttributeByCategoryId(CategoryID);
                return listofAttribute;
            }
            return listofAttribute;
        }
        public static List<SelectListItem> AttributesByIdProductAndIdCategory(int CategoryID,int ProductID)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (CategoryID > 0)
            {

                List<LiteCommerce.DomainModels.ProductAttribute> listOfAttributes = CatelogBLL.GetAttrByCategoryIdAndProductId(CategoryID, ProductID);
                foreach (LiteCommerce.DomainModels.ProductAttribute attr in listOfAttributes)
                {
                    list.Add(new SelectListItem() { Value = attr.AttributeID.ToString(), Text = attr.AttributeName });

                }
            }
            return list;
        }
        public static List<String> ListUserRoleTypes()
        {
            List<String> listOfTypeRoles = new List<string>()
            {
                UserRoleTypes.ROLE_CATALOG,
                UserRoleTypes.ROLE_MANAGER,
                UserRoleTypes.ROLE_SALE,
            };
            return listOfTypeRoles;
        }
        public static List<int> ListOrderYear()
        {
            return CatelogBLL.GetYearOrder();
        }
    }
}