﻿using LiteCommerce.BusinessLayers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerceAdmin.Controllers
{
    [Authorize]
    public class DashBoardController : Controller
    {
        // GET: DashBoard
        public ActionResult Index()
        {
            ViewBag.Title = "DashBoard";
            var model = new Models.DashBoardModel()
            {
                countCustomers = CatelogBLL.CountCustomer(),
                countEmployees = CatelogBLL.CountEmployee(),
                countOrders = CatelogBLL.CountOrder(),
                countShippers = CatelogBLL.CountShipper(),
                countProducts = CatelogBLL.CountProduct(),
                countSuppliers = CatelogBLL.CountShipper(),
                Top5Selling= CatelogBLL.Top5ProductSelling()
            };
            return View(model);
        }
    }
}