﻿using LiteCommerce.BusinessLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerceAdmin.Controllers
{
    [Authorize(Roles = UserRoleTypes.ROLE_CATALOG)]
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index(string category ="",string supplier = "", int page = 1 , string searchValue = "")
        {
            int pageSize = 5;
            int rowCount = 0;
            List<Product> listOfProduct = CatelogBLL.ListOfProduct(page, pageSize, searchValue, out rowCount,category,supplier);
            var model = new Models.ProductPaginationResult()
            {
                Page = page,
                PageSize = pageSize,
                RowCount = rowCount,
                SearchValue = searchValue,
                Data = listOfProduct,
                Category = category,
                Supplier = supplier
            };
            return View(model);
        }
        public ActionResult Input(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                ViewBag.Title = "Create new Employee";
                Product newProduct = new Product()
                {
                    ProductID = 0
                };
                return View(newProduct);
            }
            else
            {
                ViewBag.Title = "Edit a Product";
                Product editProduct = CatelogBLL.GetProduct(Convert.ToInt32(id));
                if (editProduct == null)
                    return RedirectToAction("Index");
                return View(editProduct);
            }
        }
        [HttpPost]
        public ActionResult Input(string[] keyAttr,string[] nameAttr,Product product , HttpPostedFileBase PhotoPath)
        {
            try
            {
                if (string.IsNullOrEmpty(product.ProductName))
                    ModelState.AddModelError("ProductName", "ProductName expected");
                if(product.CategoryID == 0)
                    ModelState.AddModelError("CategoryID", "Please select a category !");
                if (product.SupplierID == 0)
                    ModelState.AddModelError("SupplierID", "Please select a supplier !");
                if (string.IsNullOrEmpty(product.QuantityPerUnit))
                    ModelState.AddModelError("QuantityPerUnit", "Quantity Per Unit expected");
                if (string.IsNullOrEmpty(product.Descriptions))
                    ModelState.AddModelError("Descriptions", "Descriptions Per Unit expected");
                if (product.UnitPrice <= 0)
                    ModelState.AddModelError("UnitPrice", "UnitPrice Per Unit expected !");
                if(keyAttr == null || keyAttr.Length <= 0 )
                    ModelState.AddModelError("ProductAttribute", "Please enter Attribute Product !");
                if (product.ProductID == 0)
                {
                    if (PhotoPath != null && PhotoPath.ContentLength > 0)
                    {
                        var ResumeName = Path.GetFileName(PhotoPath.FileName);
                        string fileName = $"{DateTime.Now.Ticks}{Path.GetExtension(PhotoPath.FileName)}";
                        string path = System.IO.Path.Combine(Server.MapPath("~/Uploads/"), fileName);
                        PhotoPath.SaveAs(path);
                        product.PhotoPath = "/Uploads/" + fileName;
                    }
                    int productId = CatelogBLL.AddProduct(product);
                    CatelogBLL.AddProductAttribute(keyAttr, nameAttr, productId);
                }
                else
                {
                    if (product.PhotoPath != null)
                    {
                        if (PhotoPath != null && PhotoPath.ContentLength > 0)
                        {
                            var ResumeName = Path.GetFileName(PhotoPath.FileName);
                            string fileName = $"{DateTime.Now.Ticks}{Path.GetExtension(PhotoPath.FileName)}";
                            string path = System.IO.Path.Combine(Server.MapPath("~/Uploads/"), fileName);
                            PhotoPath.SaveAs(path);
                            product.PhotoPath = "/Uploads/" + fileName;
                        }
                    }
                    else
                    {
                        Product editProduct = CatelogBLL.GetProduct(product.ProductID);
                        product.PhotoPath = editProduct.PhotoPath;
                    }
                    CatelogBLL.UpdateProduct(product,keyAttr,nameAttr);

                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message + ":" + ex.StackTrace);
                return View(product);
            }
            

        }

        [HttpPost]
        public ActionResult Delete(int[] productIds)
        {
            if(productIds != null)
            {
                CatelogBLL.DeleteProduct(productIds);
            }
            return RedirectToAction("Index");
        }
    }
}