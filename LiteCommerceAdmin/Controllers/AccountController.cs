﻿using LiteCommerce.BusinessLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace LiteCommerceAdmin.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        //public ActionResult ChangePassword(string email ,string oldPassword,string newPassword,string confirmPassword)
        //{
        //    if (newPassword.Equals(confirmPassword)){
        //        ModelState.AddModelError("confirmPassword", "The confirmation password is incorrect");
        //        ViewBag.confirmPassword = confirmPassword;
        //        ViewBag.oldPassword = oldPassword;
        //        ViewBag.newPassword = newPassword;
        //        return View();
        //    }
           
        //}
        public ActionResult ForgetPassword()
        {
            return View();
        }
        public ActionResult SignOut()
        {
            Session.Abandon();
            Session.Clear();
            System.Web.Security.FormsAuthentication.SignOut();
            return RedirectToAction("SignIn");
        }
        [AllowAnonymous]
        [HttpGet]
        public ActionResult SignIn()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "DashBoard");
            return View();
        }
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SignIn(string email, string password)
        {
            UserAccount user = UserAccountBLL.Authorize(email, CodeHelper.EncodeMD5(password),UserAccountTypes.EMPLOYEE);
            if(user != null)
            {
                WebUserData userData = new WebUserData()
                {
                    UserID = user.UserID,
                    FullName = user.FullName,
                    GroupName = user.roles,
                    Photo = user.Photo,
                    ClientIP = Request.UserHostAddress,
                    LoginTime = DateTime.Now,
                    SessionID = Session.SessionID,
                    Title = user.Title,
                    Email = user.Email
                };
                FormsAuthentication.SetAuthCookie(userData.ToCookieString(), false);
                return RedirectToAction("Index", "DashBoard");
            }
            else
            {
                ModelState.AddModelError("LoginError", "Login Fail");
                ViewBag.Email = email;
                return View();
            }
            //Employee emp = CatelogBLL.GetInfo(email, password);
            //if (emp != null)
            //{
            //    System.Web.Security.FormsAuthentication.SetAuthCookie(email, false);
            //    JavaScriptSerializer serializer = new JavaScriptSerializer();

            //    string userData = serializer.Serialize(emp);
            //    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
            //     1,
            //     email,
            //     DateTime.Now,
            //     DateTime.Now.AddMinutes(15),
            //     false,
            //     userData);
            //    string encTicket = FormsAuthentication.Encrypt(authTicket);
            //    HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            //    Response.Cookies.Add(faCookie);
            //    return RedirectToAction("Index", "DashBoard");
            //}
            //else
            //{
            //    ModelState.AddModelError("LoginError", "Login Fail");
            //    ViewBag.Email = email;
            //    return View();
            //}

            
        }

    }
}