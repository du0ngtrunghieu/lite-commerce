﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LiteCommerce.DomainModels;
using LiteCommerce.BusinessLayers;

namespace LiteCommerceAdmin.Controllers
{
    public class AttributeController : Controller
    {
        // GET: Attribute
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult GetAttribute(string id)
        {
            List<LiteCommerce.DomainModels.Attribute> listofAttribute = CatelogBLL.GetAttributeByCategoryId(Convert.ToInt32(id));
            return Json(listofAttribute, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetAttribute(string ProductID,string CategoryID)
        {
           if(ProductID== null)
            {
                List<LiteCommerce.DomainModels.Attribute> listofAttribute = CatelogBLL.GetAttributeByCategoryId(Convert.ToInt32(CategoryID));
                return Json(listofAttribute, JsonRequestBehavior.AllowGet);
            }else
            {
                List<LiteCommerce.DomainModels.ProductAttribute> listofAttribute = CatelogBLL.GetAttrByCategoryIdAndProductId(Convert.ToInt32(CategoryID),Convert.ToInt32(ProductID));
                return Json(listofAttribute, JsonRequestBehavior.AllowGet);
            }
        }
    }
}