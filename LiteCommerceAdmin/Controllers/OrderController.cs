﻿using LiteCommerce.BusinessLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerceAdmin.Controllers
{
    [Authorize(Roles = UserRoleTypes.ROLE_SALE)]
    public class OrderController : Controller
    {
        // GET: Order
        public ActionResult Index(int page = 1, string searchValue = "")
        {
            int pageSize = 10;
            int rowCount = 0;
            List<Order> listOfOrder = CatelogBLL.ListOfOrder(page, pageSize, searchValue, out rowCount);
            var model = new Models.OrderPaginationResult()
            {
                Page = page,
                PageSize = pageSize,
                RowCount = rowCount,
                SearchValue = searchValue,
                Data = listOfOrder
            };
            return View(model);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Input(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                ViewBag.Title = "Create new Order";
                Order order = new Order()
                {
                    OrderID = 0,
                };
                return View(order);
            }
            else
            {
                ViewBag.Title = "Order";
                ViewBag.SmallTitle = "#" + id; 

                Order editOrder = CatelogBLL.GetDetailsOrder(Convert.ToInt32(id));
                if (editOrder == null)
                    return RedirectToAction("Index");
                return View(editOrder);
            }
        }
        [HttpGet]
        public ActionResult GetYearOrder()
        {
            return Json(CatelogBLL.GetYearOrder(), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetOrderYearly(string year)
        {
            return Json(CatelogBLL.GetOrderYearly(Convert.ToInt32(year)));
        }
    }
}