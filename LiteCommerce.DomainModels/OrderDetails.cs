﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DomainModels
{
    public class OrderDetails
    {
        public int ProductID { get; set; }
        public Product Product { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
        public double Discount { get; set; }

        public string subTotal
        {
            get
            {
                decimal total = 0;
                if (Discount > 0)
                {
                    total += Quantity * (UnitPrice * Convert.ToDecimal(Discount));
                }
                else
                {
                    total += Quantity * UnitPrice;
                }
                return total.ToString("c");

            }
        }
        //public string discountConvert
        //{
        //    get
        //    {
                
        //        return Convert.ToDecimal(Discount * 100).ToString() + " %";
        //    }
        //}

        
    }
}
