﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DomainModels
{
    public enum StatusOrder
    {
        PENDING, PROCESSING, SHIPPED, RETURNED, CANCELED,DELIVERED
    }

    // PROCESSING : Ngày hiện tại < Ngày Require
    // SHIPPED : NGÀY SHIPPED == NULL 
    // DELIVERED : NGÀY SHIPPER < NGÀY REQUIRE < NGÀY HIỆN TẠI
}
