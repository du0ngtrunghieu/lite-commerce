﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DomainModels
{
    public class Order
    {
        public int OrderID { get; set; }
        public Employee Employee { get; set; }
        public int EmployeeID { get; set; }
        public Customer Customer { get; set; }
        public string CustomerID { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime RequiredDate { get; set; }
        public DateTime ShippedDate { get; set; }
        public Shipper Shipper { get; set; }
        public int ShipperID { get; set; }
        public decimal Freight { get; set; }
        public string ShipAddress { get; set; }
        public string ShipCountry { get; set; }
        public string ShipCity { get; set; }
        public List<OrderDetails> OrderDetails { get; set; }

        public string Total
        {
            get
            {
                decimal total = 0;
                OrderDetails.ForEach(x =>
                {
                    if(x.Discount > 0)
                    {
                        total += x.Quantity * (x.UnitPrice * Convert.ToDecimal(x.Discount));
                    }else
                    {
                        total += x.Quantity * x.UnitPrice;
                    }
                    
                });
                return total.ToString("c");
            }
        }
        

        public string TotalShipped
        {
            get
            {
                if(Freight > 0)
                {
                    decimal total = 0;
                    OrderDetails.ForEach(x =>
                    {
                        if (x.Discount > 0)
                        {
                            total += x.Quantity * (x.UnitPrice * Convert.ToDecimal(x.Discount));
                        }
                        else
                        {
                            total += x.Quantity * x.UnitPrice;
                        }

                    });
                    total = total + Freight;
                    return total.ToString("c");
                }else
                {
                    return Total;
                }
            }
        }
        public StatusOrder Status
        {
            get
            {
                if(ShippedDate == null)
                {
                    return StatusOrder.PROCESSING;
                }
                if(ShippedDate.CompareTo(RequiredDate) < 0)
                {
                    return StatusOrder.DELIVERED;
                }
                return StatusOrder.PENDING;
            }
        }
         
    }
}
