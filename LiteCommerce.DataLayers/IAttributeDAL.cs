﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DataLayers
{
    public interface IAttributeDAL
    {
        List<DomainModels.Attribute> GetAttributesByIdCategory(int categoryID);
        int AddAttributeProduct(int attrId, string attrValue, int productID);
        List<DomainModels.ProductAttribute> GetAttributesByIdCategoryAndIdProduct(int categoryID, int ProductID);
        int AddAttribute(int categoryId, string attributeName);
        int DeleteAttributeByCategory(int categoryID);
        int DeleteAttributeProduct(int productID);
    }
    
}
