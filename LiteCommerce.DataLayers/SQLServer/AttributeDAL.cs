﻿
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DataLayers.SQLServer
{
    public class AttributeDAL : IAttributeDAL
    {
        private string connectionString;

        public AttributeDAL(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public int AddAttributeProduct(int attrId, string attrValue, int productID)
        {
            int idProductAttr = 0;
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"INSERT INTO ProductAttributes
                                          (
	                                          ProductID,
	                                          AttributeValues,
                                              AttributeID
                                          )
                                          VALUES
                                          (
	                                          @ProductID,
	                                          @AttributeValues,
                                              @AttributeID
	                                          
                                          );
                                          SELECT @@IDENTITY;";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@ProductID", productID);
                cmd.Parameters.AddWithValue("@AttributeValues", attrValue);
                cmd.Parameters.AddWithValue("@AttributeID", attrId);
                idProductAttr = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }

            return idProductAttr;
            
        }
        public int AddAttribute(int categoryId, string attributeName)
        {
            int idAttr = 0;
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"INSERT INTO Attributes
                                          (
	                                          
	                                          AttributeName,
                                              CategoryID
                                          )
                                          VALUES
                                          (
	                                          @AttributeName,
	                                          @CategoryID
	                                          
                                          );
                                          SELECT @@IDENTITY;";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@AttributeName", attributeName);
                cmd.Parameters.AddWithValue("@CategoryID", categoryId);

                idAttr = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }

            return idAttr;

        }
        public int DeleteAttributeProduct(int productID)
        {
            int countDeleted = 0;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"DELETE FROM ProductAttributes WHERE ProductID = @productId";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.Add("@productId", SqlDbType.Int);
                cmd.Parameters["@productId"].Value = productID;
                countDeleted = cmd.ExecuteNonQuery();
                connection.Close();
            }
            return countDeleted;
        }

        public List<DomainModels.Attribute> GetAttributesByIdCategory(int categoryID)
        {
            List<DomainModels.Attribute> data = new List<DomainModels.Attribute>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"SELECT * FROM Attributes WHERE CategoryID = @categoryID";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@categoryID", categoryID);
                using (SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                    {
                        data.Add(new DomainModels.Attribute()
                        {
                            CategoryID = Convert.ToInt32(reader["CategoryID"]),
                            AttributeName = Convert.ToString(reader["AttributeName"]),
                            AttributeID = Convert.ToInt32(reader["Id"])
                        });
                    }
                }

                connection.Close();
            }

            return data;
        }
        public List<DomainModels.ProductAttribute> GetAttributesByIdCategoryAndIdProduct(int categoryID,int ProductID)
        {
            List<DomainModels.ProductAttribute> data = new List<DomainModels.ProductAttribute>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"select * from Products p 
                                    join ProductAttributes pa on p.ProductID = pa.ProductID
                                    join Attributes a on pa.AttributeID = a.Id
                                    join Categories c on p.CategoryID = c.CategoryID
                                    where c.CategoryID = @categoryID and p.ProductID =@productID";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@categoryID", categoryID);
                cmd.Parameters.AddWithValue("@productID", ProductID);
                using (SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                    {
                        data.Add(new DomainModels.ProductAttribute()
                        {
                            AttributeValue = Convert.ToString(reader["AttributeValues"]),
                            AttributeName = Convert.ToString(reader["AttributeName"]),
                            AttributeID = Convert.ToInt32(reader["AttributeID"])
                        });
                    }
                }

                connection.Close();
            }

            return data;
        }

        public int DeleteAttributeByCategory(int categoryID)
        {
            int countDeleted = 0;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"DELETE FROM Attributes WHERE CategoryID = @categoryID AND (CategoryID NOT IN(SELECT CategoryID FROM Products))";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.Add("@categoryID", SqlDbType.Int);
                cmd.Parameters["@categoryID"].Value = categoryID;
                countDeleted = cmd.ExecuteNonQuery();
                connection.Close();
            }
            return countDeleted;
        }
    }
}
