﻿using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace LiteCommerce.DataLayers.SQLServer
{
    public class ProductDAL : IProductDAL
    {
        private string connectionString;
        public ProductDAL(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public int Add(Product data)
        {
            int productId = 0;
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"INSERT INTO Products
                                          (
	                                          ProductName,
	                                          SupplierID,
	                                          CategoryID,
                                              QuantityPerUnit,
                                              UnitPrice,
                                              Descriptions,
	                                          PhotoPath
                                          )
                                          VALUES
                                          (
	                                          @ProductName,
	                                          @SupplierID,
	                                          @CategoryID,
                                              @QuantityPerUnit,
                                              @UnitPrice,
                                              @Descriptions,
	                                          @PhotoPath
                                          );
                                          SELECT @@IDENTITY;";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                
                cmd.Parameters.AddWithValue("@ProductName", data.ProductName);
                cmd.Parameters.AddWithValue("@SupplierID", data.SupplierID);
                cmd.Parameters.AddWithValue("@CategoryID", data.CategoryID);
                cmd.Parameters.AddWithValue("@QuantityPerUnit", data.QuantityPerUnit);
                cmd.Parameters.AddWithValue("@UnitPrice", data.UnitPrice);
                cmd.Parameters.AddWithValue("@Descriptions", data.Descriptions);
                cmd.Parameters.AddWithValue("@PhotoPath", data.PhotoPath);
                productId = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }

            return productId;
        }

        public int Count(string searchValue,string categoryId,string supplierId)
        {
            int count = 0;
            List<Customer> data = new List<Customer>();
            if (!string.IsNullOrEmpty(searchValue))
                searchValue = "%" + searchValue + "%";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"
                                select count(*)
	                                from Products
	                               where (@searchValue = N'' or ProductName like @searchValue) and ((@categoryId = N'' or CategoryID like @categoryId) and (@supplierId = N'' or SupplierID like @supplierId))
                                    ";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@searchValue", searchValue);
                cmd.Parameters.AddWithValue("@categoryId", categoryId);
                cmd.Parameters.AddWithValue("@supplierId", supplierId);
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }

        public int Delete(int[] ProductIDs)
        {
            int countDeleted = 0;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"DELETE FROM Products
                                            WHERE(ProductID = @ProductID)
                                            AND(ProductID NOT IN(SELECT ProductID FROM OrderDetails))
                                    ";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.Add("@ProductID", SqlDbType.Int);
                foreach (int productId in ProductIDs)
                {
                    cmd.Parameters["@ProductID"].Value = productId;
                    int rowsAffected = cmd.ExecuteNonQuery();
                    if (rowsAffected > 0)
                        countDeleted += 1;
                }

                connection.Close();
            }
            return countDeleted;

        }

        public Product Get(int ProductID)
        {
            Product data = null;
            
            List<ProductAttribute> dataProductAttrs = new List<ProductAttribute>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand();
                
                cmd.CommandText = @"select * from Products where ProductID = @productID";
                
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@productID", ProductID);

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbReader.Read())
                    {
                        data = new Product()
                        {
                            ProductID = Convert.ToInt32(dbReader["ProductID"]),
                            ProductName = Convert.ToString(dbReader["ProductName"]),
                            Descriptions = Convert.ToString(dbReader["Descriptions"]),
                            CategoryID = Convert.ToInt32(dbReader["CategoryID"]),
                            PhotoPath = Convert.ToString(dbReader["PhotoPath"]),
                            QuantityPerUnit = Convert.ToString(dbReader["QuantityPerUnit"]),
                            SupplierID = Convert.ToInt32(dbReader["SupplierID"]),
                            UnitPrice = Convert.ToDecimal(dbReader["UnitPrice"])


                        };
                    }
                }

                connection.Close();


            }
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand cmd2 = new SqlCommand();
                cmd2.CommandText = @"select * from Products p 
                                    join ProductAttributes pa on p.ProductID = pa.ProductID
                                    join Attributes a on pa.AttributeID = a.Id
                                    where p.ProductID = @productID";
                cmd2.CommandType = CommandType.Text;
                cmd2.Connection = connection;
                cmd2.Parameters.AddWithValue("@productID", ProductID);
                using (SqlDataReader reader = cmd2.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                    {
                        dataProductAttrs.Add(new ProductAttribute
                        {
                            AttributeID = Convert.ToInt32(reader["AttributeID"]),
                            AttributeName = Convert.ToString(reader["AttributeName"]),
                            AttributeValue = Convert.ToString(reader["AttributeValues"])
                  
                        });
                    }
                }

                connection.Close();

            }
            data.ProductAttributes = dataProductAttrs;
            return data;
        }

       

        public List<Product> List(int page, int pagesize, string searchValue,string categoryId,string supplierId)
        {
            List<Product> data = new List<Product>();
            if (!string.IsNullOrEmpty(searchValue))
                searchValue = "%" + searchValue + "%";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"
                                select * 
                                from (
	                                select ROW_NUMBER() over(order by ProductName) as RowNumber,Products.* 
	                                from Products
	                                where (@searchValue = N'' or ProductName like @searchValue) and (@categoryId = N'' or CategoryID like @categoryId) and (@supplierId = N'' or SupplierID like @supplierId)
	                                ) as t
                                where t.RowNumber between (@page -1) * @pageSize +1 and (@page*@pageSize)
                                order by t.RowNumber
                                    ";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@page", page);
                cmd.Parameters.AddWithValue("@pageSize", pagesize);
                cmd.Parameters.AddWithValue("@searchValue", searchValue);
                cmd.Parameters.AddWithValue("@categoryId", categoryId);
                cmd.Parameters.AddWithValue("@supplierId", supplierId);
                using (SqlDataReader dbReader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new Product()
                        {

                            ProductID = Convert.ToInt32(dbReader["ProductID"]),
                            ProductName = Convert.ToString(dbReader["ProductName"]),
                            Descriptions = Convert.ToString(dbReader["Descriptions"]),
                            CategoryID = Convert.ToInt32(dbReader["CategoryID"]),
                            PhotoPath = Convert.ToString(dbReader["PhotoPath"]),
                            QuantityPerUnit = Convert.ToString(dbReader["QuantityPerUnit"]),
                            SupplierID = Convert.ToInt32(dbReader["SupplierID"]),
                            UnitPrice = Convert.ToDecimal(dbReader["UnitPrice"])


                        });
                    }
                }
                connection.Close();
            }
            return data;
        }

        public bool Update(Product data)
        {
            int rowsAffected = 0;
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"UPDATE Products
                                           SET ProductName = @ProductName
	                                          ,SupplierID= @SupplierID
	                                          ,CategoryID = @CategoryID
                                              ,QuantityPerUnit= @QuantityPerUnit
                                              ,UnitPrice= @UnitPrice
                                              ,Descriptions= @Descriptions
	                                          ,PhotoPath= @PhotoPath
                                           WHERE ProductID = @ProductID";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@ProductID", data.ProductID);
                cmd.Parameters.AddWithValue("@ProductName", data.ProductName);
                cmd.Parameters.AddWithValue("@SupplierID", data.SupplierID);
                cmd.Parameters.AddWithValue("@CategoryID", data.CategoryID);
                cmd.Parameters.AddWithValue("@QuantityPerUnit", data.QuantityPerUnit);
                cmd.Parameters.AddWithValue("@UnitPrice", data.UnitPrice);
                cmd.Parameters.AddWithValue("@Descriptions", data.Descriptions);
                cmd.Parameters.AddWithValue("@PhotoPath", data.PhotoPath);
                rowsAffected = Convert.ToInt32(cmd.ExecuteNonQuery());
                connection.Close();
            }

            return rowsAffected > 0;
        }
        public List<Product> GetTop5SellingProducts()
        {
            List<Product> data = new List<Product>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"select TOP 5 count(o.OrderID) as Numb ,p.ProductID ,p.ProductName,p.PhotoPath,p.Descriptions,p.UnitPrice from Products p join OrderDetails o on p.ProductID = o.ProductID
                                    group by p.ProductID , p.ProductName,p.PhotoPath,p.Descriptions,p.UnitPrice
                                    ORDER BY Numb DESC
                                    ";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                
                using (SqlDataReader dbReader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new Product()
                        {

                            ProductID = Convert.ToInt32(dbReader["ProductID"]),
                            ProductName = Convert.ToString(dbReader["ProductName"]),
                            PhotoPath = Convert.ToString(dbReader["PhotoPath"]),
                            Descriptions = Convert.ToString(dbReader["Descriptions"]),
                            UnitPrice = Convert.ToDecimal(dbReader["UnitPrice"]),

                        });
                    }
                }
                connection.Close();
            }
            return data;
        }
    }
}
