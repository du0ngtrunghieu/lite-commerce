﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiteCommerce.DomainModels;
using System.Data.SqlClient;
using System.Data;

namespace LiteCommerce.DataLayers.SQLServer
{
    public class OrderDAL : IOrderDAL
    {
        private string connectionString;

        public OrderDAL(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public int Count(string searchValue)
        {
            int count = 0;
            List<Order> data = new List<Order>();
            if (!string.IsNullOrEmpty(searchValue))
                searchValue = "%" + searchValue + "%";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"
                                select count(DISTINCT o.OrderID)
                                        from Orders o join OrderDetails od on o.OrderID = od.OrderID
                                        join Products p on od.ProductID = p.ProductID
                                        join Customers c on c.CustomerID = o.CustomerID
                                        join Employees e on e.EmployeeID = o.EmployeeID
                                        join Shippers s on s.ShipperID = o.ShipperID
	                               where (@searchValue = N'' or p.ProductName like @searchValue) 
                                    ";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@searchValue", searchValue);
                
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }

        public Order Get(int orderId)
        {
            Order data = null;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"SELECT * FROM Orders WHERE OrderID = @orderId";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@orderId", orderId);

                using (SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbReader.Read())
                    {
                        
                        data = new Order()
                        {
                            CustomerID = Convert.ToString(dbReader["CustomerID"]),
                            EmployeeID = Convert.ToInt32(dbReader["EmployeeID"]),
                            Freight = Convert.ToDecimal(dbReader["Freight"]),
                            OrderDate = Convert.ToDateTime(dbReader["OrderDate"]),
                            OrderID = Convert.ToInt32(dbReader["OrderID"]),
                            RequiredDate = Convert.ToDateTime(dbReader["RequiredDate"]),
                            ShipAddress = Convert.ToString(dbReader["ShipAddress"]),
                            ShipCity = Convert.ToString(dbReader["ShipCity"]),
                            ShipCountry = Convert.ToString(dbReader["ShipCountry"]),

                            ShippedDate = Convert.ToDateTime(dbReader["ShippedDate"] != null ? dbReader["ShippedDate"] : 0 ),
                            ShipperID = Convert.ToInt32(dbReader["ShipperID"])

                        };
                    }
                }

                connection.Close();
            }
            return data;
        }

        public List<OrderDetails> GetDetailsByOrderId(int orderId)
        {
            List<OrderDetails> data = new List<OrderDetails>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"select * from OrderDetails od join Products p on od.ProductID = P.ProductID where od.OrderID = @orderId";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@orderId", orderId);
                using (SqlDataReader dbReader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new OrderDetails()
                        {
                            Discount = Convert.ToDouble(dbReader["Discount"]),
                            ProductID = Convert.ToInt32(dbReader["ProductID"]),
                            Quantity = Convert.ToInt32(dbReader["Quantity"]),
                            UnitPrice = Convert.ToDecimal(dbReader["UnitPrice"]),
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }

        public List<Order> List(int page, int pagesize, string searchValue)
        {
            List<Order> data = new List<Order>();
            if (!string.IsNullOrEmpty(searchValue))
                searchValue = "%" + searchValue + "%";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"
                                select * 
                                from (
	                                select DISTINCT o.OrderID ,o.CustomerID , o.EmployeeID , o.Freight,o.OrderDate,o.RequiredDate,o.ShipAddress,o.ShipCity,o.ShipCountry,o.ShippedDate,o.ShipperID ,DENSE_RANK() over(order by o.OrderID) as RowNumber
                                        from Orders o join OrderDetails od on o.OrderID = od.OrderID
                                            join Products p on od.ProductID = p.ProductID
                                            join Customers c on c.CustomerID = o.CustomerID
                                            join Employees e on e.EmployeeID = o.EmployeeID
                                            join Shippers s on s.ShipperID = o.ShipperID
	                                where (@searchValue = N'' or p.ProductName like @searchValue)
	                                ) as t
                                where t.RowNumber between (@page -1) * @pageSize +1 and (@page*@pageSize)
                                order by t.RowNumber
                                    ";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@page", page);
                cmd.Parameters.AddWithValue("@pageSize", pagesize);
                cmd.Parameters.AddWithValue("@searchValue", searchValue);
                
                using (SqlDataReader dbReader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                {
                    
                    while (dbReader.Read())
                    {
                        

                        data.Add(new Order()
                        {
                            CustomerID = Convert.ToString(dbReader["CustomerID"]),
                            EmployeeID = Convert.ToInt32(dbReader["EmployeeID"]),
                            Freight = Convert.ToDecimal(dbReader["Freight"]),
                            OrderDate = Convert.ToDateTime(dbReader["OrderDate"]),
                            OrderID = Convert.ToInt32(dbReader["OrderID"]),
                            RequiredDate = Convert.ToDateTime(dbReader["RequiredDate"]),
                            ShipAddress = Convert.ToString(dbReader["ShipAddress"]),
                            ShipCity = Convert.ToString(dbReader["ShipCity"]),
                            ShipCountry = Convert.ToString(dbReader["ShipCountry"]),
                            ShippedDate = Convert.ToDateTime(dbReader["ShippedDate"]),
                            ShipperID = Convert.ToInt32(dbReader["ShipperID"])
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }

        public List<Product> ListProductInOrder(int orderId)
        {
            List<Product> data = new List<Product>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"select * from OrderDetails od join Products p on od.ProductID = P.ProductID where od.OrderID = @orderId";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@orderId", orderId);
                using (SqlDataReader dbReader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new Product()
                        {

                            ProductID = Convert.ToInt32(dbReader["ProductID"]),
                            ProductName = Convert.ToString(dbReader["ProductName"]),
                            Descriptions = Convert.ToString(dbReader["Descriptions"]),
                            CategoryID = Convert.ToInt32(dbReader["CategoryID"]),
                            PhotoPath = Convert.ToString(dbReader["PhotoPath"]),
                            QuantityPerUnit = Convert.ToString(dbReader["QuantityPerUnit"]),
                            SupplierID = Convert.ToInt32(dbReader["SupplierID"]),
                            UnitPrice = Convert.ToDecimal(dbReader["UnitPrice"])


                        });
                    }
                }
                connection.Close();
            }
            return data;
        }

        public List<int> GetYearOrder()
        {
            List<int> yearOrder = new List<int>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"SELECT YEAR(OrderDate) YEAR FROM Orders GROUP BY YEAR(OrderDate) Order By YEAR(OrderDate)";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                using (SqlDataReader dbReader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        yearOrder.Add(Convert.ToInt32(dbReader["YEAR"]));
                    }
                }
                connection.Close();
            }
            return yearOrder;
        }
        public List<OrderYearly> GetOrderYearly(int year)
        {
            List<OrderYearly> data = new List<OrderYearly>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = @"SELECT MONTH(OrderDate) MONTH, COUNT(*) COUNT
                                    FROM Orders
                                    WHERE YEAR(OrderDate)=@year
                                    GROUP BY MONTH(OrderDate)
                                    ";
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@year", year);
                using (SqlDataReader dbReader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new OrderYearly()
                        {

                            Month = Convert.ToInt32(dbReader["Month"]),
                            Count = Convert.ToInt32(dbReader["Count"])
                        });
                    }
                }
                connection.Close();
            }
            return data;
        }
    }
}
