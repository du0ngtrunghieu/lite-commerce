﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiteCommerce.DomainModels;

namespace LiteCommerce.DataLayers.SQLServer
{
    public class EmployeeUserAccountDAL : IUserAccountDAL
    {
        public string connectionString { get; set; }
        public EmployeeUserAccountDAL (string connectionString)
        {
            this.connectionString = connectionString;
        }
        private static IEmployeeDAL EmployeeDB { get; set; }
        public UserAccount Authorize(string userName, string password)
        {
            EmployeeDB = new EmloyeeDAL(connectionString);
            Employee emp =  EmployeeDB.GetInfoUserByEmailAndPass(password, userName);
            UserAccount user = new UserAccount();
            if (emp != null)
            {
                user.Email = emp.Email;
                user.UserID = Convert.ToString(emp.EmployeeID);
                user.Photo = emp.PhotoPath;
                user.Title = emp.Title;
                user.FullName = emp.FirstName + emp.LastName;
                user.roles = emp.roles;
                return user;
            }
            return null;
        }

        public bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }
    }
}
