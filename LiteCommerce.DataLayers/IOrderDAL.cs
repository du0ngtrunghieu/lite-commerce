﻿using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DataLayers
{
    public interface IOrderDAL
    {
        List<Order> List(int page, int pagesize, string searchvalue);
        int Count(string searchvalue);
        List<Product> ListProductInOrder(int orderId);
        List<OrderDetails> GetDetailsByOrderId(int orderId);
        List<int> GetYearOrder();
        Order Get(int orderId);
        List<OrderYearly> GetOrderYearly(int year);
    }
}
