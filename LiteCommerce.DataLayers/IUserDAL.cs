﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DataLayers
{
    public interface IUserDAL : IPrincipal
    {
         int EmployeeID { get; set; }
         string LastName { get; set; }
         string FirstName { get; set; }
         string Title { get; set; }
         DateTime BirthDate { get; set; }
         DateTime HireDate { get; set; }
         string Email { get; set; }
         string Address { get; set; }
         string City { get; set; }

         string Country { get; set; }
         string HomePhone { get; set; }
         string Notes { get; set; }
         string PhotoPath { get; set; }
         string Password { get; set; }
    }
}
