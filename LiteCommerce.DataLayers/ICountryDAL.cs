﻿using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DataLayers
{
    public interface ICountryDAL
    {
        List<Country> getAll();
        List<Country> List(int page, int pagesize, string searchvalue);
        Country Get(int countryId);
        int Add(Country country);
        int Count(string searchvalue);
        bool Update(Country data);
        int Delete(int[] CountryIds);

        

    }
}
